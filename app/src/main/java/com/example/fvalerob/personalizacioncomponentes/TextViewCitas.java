package com.example.fvalerob.personalizacioncomponentes;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Valero on 16/11/2017.
 */

public class TextViewCitas extends android.support.v7.widget.AppCompatTextView {

    private Random citaAleatoria;
    private String[] arrayCitas;
    private void inicializar(){
        citaAleatoria = new Random();
        arrayCitas = getResources().getStringArray(R.array.listadoCitas);
        elegirCita();
    }
    private void elegirCita(){
        int numAleatorio = citaAleatoria.nextInt(arrayCitas.length);
        //Cargamos en el textView una de las citas elegidas aleatoriamente.
        setText(arrayCitas[numAleatorio]);
    }
    public TextViewCitas (Context context, AttributeSet ats, int defStyle){
        super(context,ats,defStyle);
        inicializar();
    }
    public TextViewCitas(Context context) {
        super(context);
        inicializar();
    }
    public TextViewCitas(Context context, AttributeSet attrs){
        super(context,attrs);
        inicializar();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event){
        elegirCita();
        return super.onTouchEvent(event);
    }
}
