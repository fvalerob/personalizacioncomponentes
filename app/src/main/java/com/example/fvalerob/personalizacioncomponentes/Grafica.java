package com.example.fvalerob.personalizacioncomponentes;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Valero on 17/11/2017.
 */

public class Grafica extends View{
    //Definimos el tamaño predefinido.
    private final int DEFAULT_SIZE = 50;
    //Rojo el sector correspondiente indicado
    private Paint paintRojo;
    //Azul el resto
    private Paint paintAzul;
    private int percentage = 0;
    private RectF mOval;
    private float centerX;
    private float centerY;
    private float radius;

    public void inicializar(AttributeSet attrs) {
        if(attrs == null)
            return;
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.Grafica);
        this.percentage = ta.getInt(R.styleable.Grafica_percentage, 0);

        paintRojo = new Paint();
        paintRojo.setStyle(Paint.Style.FILL);
        paintRojo.setAntiAlias(true);
        paintRojo.setColor(Color.RED);

        paintAzul = new Paint();
        paintAzul.setStyle(Paint.Style.FILL);
        paintAzul.setAntiAlias(true);
        paintAzul.setColor(Color.BLUE);

        mOval = new RectF();
    }

    public Grafica(Context context) {
        super(context);
        inicializar(null);
    }

    public Grafica(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inicializar(attrs);
    }

    public Grafica(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inicializar(attrs);

    }
    public void setPercentage(int value) {
        percentage = value;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int width = DEFAULT_SIZE;
        int height = DEFAULT_SIZE;
        switch(widthMode) {
            case MeasureSpec.EXACTLY:
                width = widthSize;
                break;
            case MeasureSpec.AT_MOST:
                if(width > widthSize)
                    width = widthSize;
                break;
        }
        height = width;  // Forzamos a que el área sea cuadrada

        switch(heightMode) {
            case MeasureSpec.EXACTLY:
                height = heightSize;
                width = heightSize; // Forzamos a que el área sea cuadrada
                break;
            case MeasureSpec.AT_MOST:
                if(height > heightSize) {
                    height = heightSize;
                    width = heightSize; // Forzamos a que el área sea cuadrada
                }
                break;
        }
        centerX = width / 2f;
        centerY = height / 2f;
        radius = Math.min(width, height) / 2;

        this.setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mOval = new RectF(centerX - radius, centerY - radius, centerX + radius, centerY + radius);

        float anguloQuesito = ((float)percentage*360f)/100f;
        canvas.drawArc(mOval, 0, anguloQuesito, true, paintRojo);
        canvas.drawArc(mOval, anguloQuesito, 360-anguloQuesito, true, paintAzul);

    }
}
