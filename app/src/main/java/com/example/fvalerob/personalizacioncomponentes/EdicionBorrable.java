package com.example.fvalerob.personalizacioncomponentes;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by Valero on 16/11/2017.
 */

public class EdicionBorrable extends LinearLayout{

    public void inicializar() {
        // Creamos la interfaz a partir del layout
        LayoutInflater li = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        li.inflate(R.layout.edicionborrable, this, true);

        // Obtenemos las referencias a las vistas hijas
        Button button = (Button)findViewById(R.id.button);
        final EditText editText = (EditText)findViewById(R.id.editText);
        //onClickButton
        button.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("");
            }
        });
    }
    public EdicionBorrable(Context context) {
        super(context);
        inicializar();
    }

    public EdicionBorrable(Context context, AttributeSet attrs) {
        super(context, attrs);
        inicializar();
    }

    public EdicionBorrable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inicializar();
    }
}
